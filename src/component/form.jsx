import React, {useState} from 'react';
import { Formik, Form, Field, ErrorMessage } from 'formik';

const LoanForm = () => {
    const initialValues = {
        amount: 0,
        duration: 0,
        lastName: '',
        email: '',
        phone: ''
    };

    const [loanOffers, setLoanOffers] = useState([]);

    const handleSubmit = async (values) => {
        const payload = {
            ...values,
            amount: parseInt(values.amount, 10),
            duration: parseInt(values.duration, 10),
        };

        const data = await fetch('http://localhost:8000/api/compare', {
            method: 'POST',
            body: JSON.stringify(payload),
            headers: {
                'Content-Type': 'application/json',
                'Accept': 'application/json'
            },
            mode: 'cors'
        });

        setLoanOffers(await data.json());
    };

    const validate = (values) => {
        const errors = {};

        if (!values.amount) {
            errors.amount = 'Veuillez sélectionner un montant de prêt';
        }

        if (!values.duration) {
            errors.duration = 'Veuillez sélectionner une durée de prêt';
        }

        if (!values.lastName) {
            errors.lastName = 'Veuillez saisir votre nom';
        }

        if (!values.email) {
            errors.email = 'Veuillez saisir votre email';
        } else if (!/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,}$/i.test(values.email)) {
            errors.email = 'Adresse email invalide';
        }

        if (!values.phone) {
            errors.phone = 'Veuillez saisir votre numéro de téléphone';
        }

        return errors;
    };

    return (
        <>
            <Formik initialValues={initialValues} onSubmit={handleSubmit} validate={validate}>
                {({ isSubmitting }) => (
                    <Form>
                        <div className="form-group">
                            <label htmlFor="amount">Montant du prêt :</label>
                            <Field as="select" name="amount" id="amount" className="form-control">
                                <option value="">Sélectionnez un montant</option>
                                <option value="50000">50k</option>
                                <option value="100000">100k</option>
                                <option value="200000">200k</option>
                                <option value="500000">500k</option>
                            </Field>
                            <ErrorMessage name="loanAmount" component="div" className="text-danger" />
                        </div>
                        <div className="form-group">
                            <label htmlFor="duration">Durée du prêt en années :</label>
                            <Field as="select" name="duration" id="duration" className="form-control">
                                <option value="">Sélectionnez une durée</option>
                                <option value="15">15 ans</option>
                                <option value="20">20 ans</option>
                                <option value="25">25 ans</option>
                            </Field>
                            <ErrorMessage name="duration" component="div" className="text-danger" />
                        </div>
                        <div className="form-group">
                            <label htmlFor="lastName">Nom :</label>
                            <Field type="text" name="lastName" id="lastName" className="form-control" />
                            <ErrorMessage name="lastName" component="div" className="text-danger" />
                        </div>
                        <div className="form-group">
                            <label htmlFor="email">Email :</label>
                            <Field type="email" name="email" id="email" className="form-control" />
                            <ErrorMessage name="email" component="div" className="text-danger" />
                        </div>
                        <div className="form-group">
                            <label htmlFor="phone">Téléphone :</label>
                            <Field type="tel" name="phone" id="phone" className="form-control" />
                            <ErrorMessage name="phone" component="div" className="text-danger" />
                        </div>
                        <button type="submit" className="btn btn-primary" disabled={isSubmitting}>Soumettre</button>
                    </Form>
                )}
            </Formik>
            {loanOffers.length > 0 && (
                <div className="loan-offers">
                    <h3>Meilleures offres de prêt :</h3>
                    <table className="table">
                        <thead>
                        <tr>
                            <th>Banque</th>
                            <th>Montant</th>
                            <th>Durée</th>
                            <th>Taux</th>
                            <th>Coût Total</th>
                        </tr>
                        </thead>
                        <tbody>
                        {loanOffers.map((offer, index) => (
                            <tr key={index}>
                                <td>{offer.bankName}</td>
                                <td>{offer.amount} €</td>
                                <td>{offer.duration} ans</td>
                                <td>{offer.rate}%</td>
                                <td>{offer.totalCost.toFixed(2)} €</td>
                            </tr>
                        ))}
                        </tbody>
                    </table>
                </div>
            )}
        </>
    );
};

export default LoanForm;
