import React, { useState } from 'react';
import { Formik, Form, Field, ErrorMessage } from 'formik';

const LoanForm = () => {
    const [loanOffers, setLoanOffers] = useState([]); // État pour stocker les offres de prêt
    // ... initial values, handleSubmit, validate ...

    const handleSubmit = async (values) => {
        // ... la requête
        const result = await data.json(); // Assure-toi de convertir la réponse en JSON
        setLoanOffers(result); // Mettre à jour l'état avec les offres de prêt
    };

    // ... validate ...

    return (
        <>
            <Formik initialValues={initialValues} onSubmit={handleSubmit} validate={validate}>
                {/* ... Ton formulaire ici ... */}
            </Formik>
            {loanOffers.length > 0 && (
                <div className="loan-offers">
                    <h3>Meilleures offres de prêt :</h3>
                    <table className="table">
                        <thead>
                            <tr>
                                <th>Banque</th>
                                <th>Montant</th>
                                <th>Durée</th>
                                <th>Taux</th>
                                <th>Coût Total</th>
                            </tr>
                        </thead>
                        <tbody>
                            {loanOffers.map((offer, index) => (
                                <tr key={index}>
                                    <td>{offer.bank}</td>
                                    <td>{offer.amount}</td>
                                    <td>{offer.duration} ans</td>
                                    <td>{offer.rate}%</td>
                                    <td>{offer.totalCost}</td>
                                </tr>
                            ))}
                        </tbody>
                    </table>
                </div>
            )}
        </>
    );
};

export default LoanForm;
