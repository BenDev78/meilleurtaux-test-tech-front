import './App.css';
import Form from "./component/form";

function App() {
  return (
    <div className="App">
        <div className={'container'}>
            <h1 className={"mb-5"}>Comparer un crédit à la consommation</h1>
            <Form></Form>
        </div>
    </div>
  );
}

export default App;
